<?php

class LogoutNotWorkingException extends Exception
{
    public function __construct($message = "Error Processing Logout. User keep logged in even after logout.", $code = 1, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}