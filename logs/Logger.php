<?php

require_once __DIR__ . '/../utils/StringUtils.php';

class Logger
{
    private static function getCurrentLogFilename(): string
    {
        return 'log';
    }

    private static function getCurrentAccessLogFilename(): string
    {
        return 'access_log';
    }

    private static function getLogDirectory(): string
    {
        return __DIR__ . '/log_files';
    }

    private static function buildFingerprint(): string
    {
        return '[' . date('Y-m-d H:i:s T') . '] ';
    }

    private static function buildFilenameDateSuffix(): string
    {
        return date('Y-m-d');
    }

    public static function log(?string $data): void
    {
        self::_log($data);
    }

    public static function logError(?string $data): void
    {
        self::_log('[ERROR] ' . $data);
    }

    public static function logAccess(?string $data): void
    {
        self::_log($data, self::getCurrentAccessLogFilename());
    }

    private static function buildLogFilename(string $logName): string
    {
        return $logName . '_' . self::buildFilenameDateSuffix() . '.txt';
    }

    private static function _log(?string $data, $logFile = null, $mode = 'a'): void
    {
        $data = self::buildFingerprint() . ($data ?? '');

        $logFile = self::getLogDirectory() . '/' . self::buildLogFilename($logFile ?? self::getCurrentLogFilename());

        self::removeOldLogFiles();

        // remove breaklines
        $data = StringUtils::removeBreaklines($data);
        $data .= PHP_EOL;

        $fh = fopen($logFile, $mode);
        if (! empty($data)) {
            fwrite($fh, $data);
        }
        fclose($fh);
    }
    
    private static function removeOldLogFiles()
    {
        $directory = self::getLogDirectory();
        $currentLogFile = self::buildLogFilename(self::getCurrentLogFilename());
        $currentAccessLogFile = self::buildLogFilename(self::getCurrentAccessLogFilename());

        if (is_dir($directory)) {
            $files = scandir($directory);

            if ($files !== false) {
                $files = array_diff($files, array('.', '..', $currentLogFile, $currentAccessLogFile, 'readme.md'));
                $files = array_filter($files, function ($file) use ($directory) {
                    return is_file($directory . '/' . $file);
                });

                foreach ($files as $file) {
                    $filename = $directory . '/' . $file;
                    if (
                        file_exists($filename)
                        && unlink($filename)
                    ) {
                        self::log("File $file has been deleted.");
                    }
                }
            }
        }
    }
}