<?php
require_once __DIR__ . '/../logs/Logger.php';

try {
    require_once __DIR__ . '/../routes/Route.php';
    require_once __DIR__ . '/../utils/Request.php';
    require_once __DIR__ . '/../utils/Response.php';
    require_once __DIR__ . '/../routes/routes.php';
    require_once __DIR__ . '/../Env.php';
   
    Env::init();

    $uri = Request::getUri();

    Logger::logAccess(Request::getMethod() . ' ' . $uri);
    
    // take out url params to get only url path 
    $uriWithoutParams = strtok($uri, '?');

    $route = Route::getRouteByName($uriWithoutParams);
    
    if (empty($route)) {
        Response::responseNotFound();
    } else {
        $route->route();
    }
} catch (\Throwable $e) {
    Logger::logError($uri . ':' . $e->getMessage() . ' :: ' . $e->getFile() . ':' . $e->getLine());
    Response::responseUnexpectedError();
}

exit;