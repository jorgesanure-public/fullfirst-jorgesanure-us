<?php

require_once __DIR__ . '/Middleware.php';
require_once __DIR__ . '/../utils/Request.php';

class Route
{
    private $routeName;
    private $callback;
    private $method;
    private static $routes;
    private $middlewares;
    private $_showOnSitemap;
    private $_requireSession;

    public const SHOW_ON_SITEMAP = true;
    public const NO_SHOW_ON_SITEMAP = false;
    public const REQUIRE_SESSION = true;
    public const NO_REQUIRE_SESSION = false;

    public function __construct($method, $routeName, $callback, $showOnSitemap = true, $requireSession = true)
    {
        $this->routeName = $routeName;
        $this->callback = $callback;
        $this->method = $method;
        $this->_showOnSitemap = $showOnSitemap ?? true;
        $this->_requireSession = $requireSession ?? true;

        if (! is_array(self::$routes)) {
            self::$routes = [];
        }
        
        self::$routes[self::buildRouteKey($this->routeName, $this->method)] = $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function requireSession(): bool
    {
        return (bool) $this->_requireSession;
    }

    public function route(): void
    {
        if ($this->requireSession()) {
            session_start();
        }

        $middlewareCheck = empty($this->middlewares);

        if (! $middlewareCheck) {
            $middlewareCheck = true;
            foreach ($this->middlewares as $middlewareName) {
                $middleware = Middleware::get($middlewareName);
                $middlewareCheck = $middlewareCheck && ! empty($middleware) && $middleware->check();
            }
        }

        if ($middlewareCheck && is_callable($this->callback)) {
            ($this->callback)();
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    public static function get($routeName, $callback, $showOnSitemap = true, $requireSession = true): Route
    {
        $route = new Route(
            Request::GET,
            $routeName,
            $callback,
            $showOnSitemap ?? true,
            $requireSession ?? true
        );

        return $route;
    }

    public static function post($routeName, $callback, $showOnSitemap = true, $requireSession = true): Route
    {
        $route = new Route(
            Request::POST,
            $routeName,
            $callback,
            $showOnSitemap ?? true,
            $requireSession ?? true
        );

        return $route;
    }

    public function middleware(array $middlewares): void
    {
        $this->middlewares = $middlewares;
    }

    public function isPublic(): bool
    {
        return empty($this->middlewares) || ! in_array('auth', $this->middlewares);
    }

    public function showOnSitemap(): bool
    {
        return (bool) $this->_showOnSitemap && $this->isPublic() && $this->isGet();
    }

    public function isPost(): bool
    {
        return $this->getMethod() === Request::POST;
    }

    public function isGet(): bool
    {
        return $this->getMethod() === Request::GET;
    }

    public static function getAllSitemapRoutes(): array
    {
        $publicRoutes = [];
        foreach ((self::$routes ?? []) as $route) {
            if ($route->showOnSitemap()) {
                $publicRoutes[] = $route;
            }
        }
        return $publicRoutes;
    }

    public static function getAllSitemapRouteNames(): array
    {
        return array_map(function($route) { return $route->getRouteName(); }, self::getAllSitemapRoutes() ?? []);
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    private static function buildRouteKey($name, $method): string
    {
        return $method . '_' . $name;
    }

    public static function getRouteByName($name, $method = null): ?Route
    {
        $route = null;
        $routeKey = self::buildRouteKey($name, $method ?? Request::getMethod());

        if (array_key_exists($routeKey, self::$routes)) {
            $route = self::$routes[$routeKey];
        }

        return $route;
    }
}