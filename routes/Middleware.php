<?php

/**
 * Middleware is call before routing.
 * Routes invoke middleware check method.
 */

class Middleware
{
    private $name;
    private $callback;
    private static $middlewareList;

    public function __construct($name, $callback)
    {
        $this->name = $name;
        $this->callback = $callback;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function check(): bool
    {
        $passed = false;

        if (is_callable($this->callback)) {
            $passed = ($this->callback)();
        }

        return $passed;
    }

    public static function get($name): ?Middleware
    {
        $middleware = null;

        if (array_key_exists($name, self::$middlewareList)) {
            $middleware = self::$middlewareList[$name];
        }

        return $middleware;
    }

    public static function set($name, $callback): void
    {
        $middleware = new Middleware($name, $callback);

        if (! is_array(self::$middlewareList)) {
            self::$middlewareList = [];
        }
        self::$middlewareList[$name] = $middleware;
    }
}