<?php

require_once __DIR__ . '/Route.php';
require_once __DIR__ . '/Middleware.php';
require_once __DIR__ . '/../controllers/DeployController.php';
require_once __DIR__ . '/../controllers/IndexController.php';
require_once __DIR__ . '/../controllers/LoginController.php';
require_once __DIR__ . '/../controllers/SignUpController.php';
require_once __DIR__ . '/../controllers/DashboardController.php';
require_once __DIR__ . '/../utils/Auth.php';
require_once __DIR__ . '/../utils/Request.php';
require_once __DIR__ . '/../exceptions/LogoutNotWorkingException.php';

Middleware::set('auth', function () {
    $isAuthenticated = Auth::isAuthenticated();;

    if (! $isAuthenticated) {
        if (Request::isApi()) {
            Response::responseUnauthorized();
        } else {
            Request::redirectTo('/login');
        }
    }

    return $isAuthenticated;
});

Middleware::set('guest', function () {
    $isAuthenticated = Auth::isAuthenticated();;

    if ($isAuthenticated) {
        if (Request::isApi()) {
            Response::responseUnauthorized('You must to logout first.');
        } else {
            Request::redirectTo('/dashboard');
        }
    }

    return ! $isAuthenticated;
});

Route::get('/', function() {
    IndexController::index();
});

Route::get('/login', function() {
    LoginController::showLoginForm();
})->middleware(['guest']);

Route::post('/login', function() {
    LoginController::login();

    if (Auth::isAuthenticated()) {
        Request::redirectTo('/dashboard');
    } else {
        Request::redirectTo('/login');
    }
})->middleware(['guest']);

Route::post('/api/login', function() {
    LoginController::login();

    if (Auth::isAuthenticated()) {
        Response::responseOK();
    } else {
        Response::responseUnauthorized('Wrong credentials.');
    }
})->middleware(['guest']);

Route::get('/logout', function() {
    LoginController::logout();

    if (Auth::isAuthenticated()) {
        throw new LogoutNotWorkingException();
    } else {
        Request::redirectTo('/');
    }
})->middleware(['auth']);

Route::get('/api/logout', function() {
    LoginController::logout();
    
    if (Auth::isAuthenticated()) {
        throw new LogoutNotWorkingException();
    } else {
        Response::responseOK();
    }
})->middleware(['auth']);

Route::get('/signup', function() {
    SignUpController::showSignUpForm();
})->middleware(['guest']);

Route::post('/signup', function() {
    SignUpController::signup();
    
    if (Auth::isAuthenticated()) {
        Request::redirectTo('/dashboard');
    } else {
        Request::redirectTo('/signup');
    }
})->middleware(['guest']);

Route::post('/api/signup', function() {
    SignUpController::signup();
    
    if (Auth::isAuthenticated()) {
        Response::responseOK();
    } else {
        Response::responseUnexpectedError();
    }
})->middleware(['guest']);

Route::post('/deploy', function() {
    DeployController::deploy();
}, Route::NO_SHOW_ON_SITEMAP, Route::NO_REQUIRE_SESSION);

Route::get('/dashboard', function() {
    DashboardController::index();
})->middleware(['auth']);
