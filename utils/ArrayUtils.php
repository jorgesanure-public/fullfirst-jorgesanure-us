<?php

class ArrayUtils
{
    public static function getValueByKeyFromArray($key, $array)
    {
        return array_key_exists($key, $array ?? []) ? $array[$key] : null;
    }
}