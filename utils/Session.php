<?php

require_once __DIR__ . '/ArrayUtils.php';

class Session
{
    public static function get($key)
    {
        return ArrayUtils::getValueByKeyFromArray($key, $_SESSION ?? []);
    }

    public static function set($key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    public static function clear(): void
    {
        $_SESSION = [];
    }
}