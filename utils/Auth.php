<?php

require_once __DIR__ . '/Session.php';

class Auth
{
    public static function isAuthenticated(): bool
    {
        return (bool) Session::get('username');
    }

    public static function getUsername(): string
    {
        return Session::get('username') ?? '';
    }
}