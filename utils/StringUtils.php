<?php

class StringUtils
{
    public static function removeBreaklines(?string $str, $replaceWith = ''): string
    {
        if (! is_null($str)) {
            $str = preg_replace('/[\r\n]+/', $replaceWith, $str);
        }

        return $str;
    }
}