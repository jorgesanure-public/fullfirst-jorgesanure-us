<?php

class Response
{
    public static function buildResponseMessage($message)
    {
        if(
            ! empty($message)
            && (
                ! is_string($message)
                || ! empty(trim($message))
            )
        ){
            @header('Content-Type: application/json');
            echo json_encode([
                'message' => $message
            ]);
        }else{
            echo '';
        }

        exit;
    }
    
    public static function responseBadRequest($message = 'Bad Request'){
        http_response_code(400);
        self::buildResponseMessage($message);
    }
    
    public static function responseNotFound($message = 'Not Found'){
        http_response_code(404);
    }
    
    public static function responseUnauthorized($message = 'Unauthorized'){
        http_response_code(401);
        self::buildResponseMessage($message);
    }
    
    public static function responseUnexpectedErrorForUser(){
        self::responseUnexpectedError('Something went wrong. Please try again or contact support.');
    }
    
    public static function responseUnexpectedError($message = ''){
        http_response_code(500);
        self::buildResponseMessage($message);
    }
    
    public static function responseOK($message = ''){
        http_response_code(200);
        if (empty($message)) {
        } else {
            self::buildResponseMessage($message);
        }
    }
}