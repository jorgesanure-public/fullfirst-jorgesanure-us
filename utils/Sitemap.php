<?php

require_once __DIR__ . '/../routes/Route.php';
require_once __DIR__ . '/../utils/Request.php';
require_once __DIR__ . '/StringUtils.php';

class Sitemap
{
    public static function buildContent(): string
    {
        $host = Request::getHostWithSchema();
        $urlPathList = Route::getAllSitemapRouteNames();

        $urlListXml = '';
        foreach ($urlPathList as $path) {
            $urlListXml .= trim(self::buildXmlUrlTag($host, $path));
        }

        $sitemapXml = "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>$urlListXml</urlset>";

        $sitemapXml = StringUtils::removeBreaklines(trim($sitemapXml));

        return $sitemapXml;
    }

    private static function buildXmlUrlTag($host, $path): string
    {
        return "<url><loc>$host$path</loc></url>";
    }

    public static function createSitemapFile(): void
    {
        $filename = __DIR__ . '/../public/sitemap.xml';
        $content = self::buildContent();
        $fh = fopen($filename, 'w');
        fwrite($fh, $content);
        fclose($fh);
    }
}