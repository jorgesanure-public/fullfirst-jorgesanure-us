<?php

require_once __DIR__ . '/../utils/Request.php';

class Robots
{
    public static function createRobotsFile(): void
    {
        $filename = __DIR__ . '/../public/robots.txt';
        $host = Request::getHostWithSchema();

        $content = "User-agent: *
Crawl-delay: 10
Sitemap: $host/sitemap.xml";

        $fh = fopen($filename, 'w');
        fwrite($fh, $content);
        fclose($fh);
    }
}