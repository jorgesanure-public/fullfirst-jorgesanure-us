<?php

require_once __DIR__ . '/../utils/ArrayUtils.php';

class Request
{
    public const GET = 'GET';
    public const POST = 'POST';

    public static function isPost(): bool
    {
        return self::getFromServer('REQUEST_METHOD') === self::POST;
    }
    
    public static function isGet(): bool
    {
        return self::getFromServer('REQUEST_METHOD') === self::GET;
    }

    public static function getMethod(): ?string
    {
        return self::getFromServer('REQUEST_METHOD');
    }

    public static function getUri(): ?string
    {
        return self::getFromServer('REQUEST_URI');
    }

    public static function getRequestSchema(): ?string
    {
        return self::getFromServer('REQUEST_SCHEME');
    }

    public static function getHttpHost(): ?string
    {
        return self::getFromServer('HTTP_HOST');
    }

    public static function getHostWithSchema(): string
    {
        return self::getRequestSchema() . '://' . self::getHttpHost();
    }

    public static function getFromServer($key)
    {
        return ArrayUtils::getValueByKeyFromArray($key, $_SERVER);
    }

    public static function getFromPostParams($key)
    {
        return ArrayUtils::getValueByKeyFromArray($key, $_POST);
    }

    public static function getFromGetParams($key)
    {
        return ArrayUtils::getValueByKeyFromArray($key, $_GET);
    }

    public static function redirectTo(string $routeName): void
    {
        header("location: $routeName");
        exit;
    }

    public static function isApi(): bool
    {
        return strpos(self::getUri(), '/api/') !== false;
    }

    public static function getUriWithoutParams(): string
    {
        $uri = self::getUri();
        $uri = strtok($uri, '?');
        return $uri;
    }
}