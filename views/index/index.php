<?php
    require_once __DIR__ . '/../../utils/Auth.php';
?>
<div class="mt-2">
    <div class="float-start">
        Coming soon...
    </div>
    <?php if (Auth::isAuthenticated()) { ?>
        <div class="float-end">
            <div class="d-inline">
                <?= Auth::getUsername(); ?>
                <a name="logout-btn" id="logout-btn" class="btn btn-sm btn-outline-danger" href="/logout" role="button">Logout</a>
            </div>
        </div>
    <?php } else { ?>
        <div class="float-end">
            <div class="d-inline">
                <a name="login-btn" id="login-btn" class="btn btn-sm btn-outline-primary" href="/login" role="button">Login</a>
            </div>
            <div class="d-inline">
                <a name="signup-btn" id="signup-btn" class="btn btn-sm btn-outline-success" href="/signup" role="button">Sign Up</a>
            </div>
        </div>
    <?php } ?>
</div>