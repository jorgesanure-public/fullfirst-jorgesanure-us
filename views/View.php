<?php

class View
{
    private static array $paramList = [];

    public static function build(string $viewFilename, array $viewParams = []): void
    {
        if (is_array($viewParams)) {
            foreach ($viewParams as $key => $value) {
                self::set($key, $value);
            }
        }

        $viewFilename = __DIR__ . '/' . $viewFilename;

        if (file_exists($viewFilename)) {
            require_once __DIR__ . '/templates/header.php';
            require_once $viewFilename;
            require_once __DIR__ . '/templates/footer.php';
        } else {
            throw new Exception("File not found while building view: '$viewFilename'", 1);
        }
    }

    public static function get(string $paramName, $defaultValue = null)
    {
        $paramValue = $defaultValue;

        if (! is_array(self::$paramList)) {
            self::$paramList = [];
        }

        if (array_key_exists($paramName, self::$paramList)) {
            $paramValue = self::$paramList[$paramName];
        }

        return $paramValue;
    }

    public static function set(string $paramName, $paramValue): void
    {
        self::$paramList[$paramName] = $paramValue;
    }
}