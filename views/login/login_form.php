<div class="container mt-5">
    <div class="row">
        <div class="d-none d-sm-block col-sm-1 col-md-2 col-lg-3"></div>
        <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="card p-3">
                <div class="row">
                    <div class="col">
                        <h1>Login Form</h1>
                    </div>
                </div>
                <div class="row mt-4">
                    <form action="/login" method="post">
                        <div class="mb-3">
                            <label for="username" class="form-label">Username:</label>
                            <input type="text"
                            class="form-control" name="username" id="username" aria-describedby="username-help" placeholder="Username">
                            <small id="username-help" class="form-text text-muted">Username can be your name, alias or email</small>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password:</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="********">
                        </div>
                        <div class="text-center">
                            <button type="submit" name="login-btn" id="login-btn" class="btn btn-primary">Submit</button>
                            <a name="back-btn" id="back-btn" class="btn btn-secondary" href="/" role="button">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="d-none d-sm-block col-sm-1 col-md-2 col-lg-3"></div>
    </div>
</div>