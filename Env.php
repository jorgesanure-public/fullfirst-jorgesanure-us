<?php

class Env
{
    private static $envVars;

    public static function init(): void
    {
        $envFilename = __DIR__ . '/.env';

        if (file_exists($envFilename)) {
            $fh = fopen($envFilename, 'r');
            while (! feof($fh)) {
                $line = fgets($fh);
                $line = trim($line);

                $pattern = '/^([^=]+)=(.+)/';

                if (preg_match($pattern, $line, $matches)) {
                    Env::set($matches[1], $matches[2]);
                }
            }
        }
    }

    public static function get(string $key, $default = null)
    {
        $envVar = $default;

        if (! is_array(self::$envVars)) {
            self::$envVars = [];
        }

        if (array_key_exists($key, self::$envVars)) {
            $envVar = self::$envVars[$key];
        }

        return $envVar;
    }

    public static function set(string $key, $value): void
    {
        self::$envVars[$key] = $value;
    }
}