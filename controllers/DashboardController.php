<?php

require_once __DIR__ . '/../views/View.php';

class DashboardController
{
    public static function index(): void
    {
        View::build('dashboard/index.php');
    }
}