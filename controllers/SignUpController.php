<?php

require_once __DIR__ . '/../views/View.php';
require_once __DIR__ . '/../utils/Request.php';

class SignUpController
{
    public static function showSignUpForm(): void
    {
        View::build('signup/signup_form.php');
    }

    public static function signup(): void
    {
        $givenUsername = Request::getFromPostParams('username');
        $givenPassword = Request::getFromServer('password');

        $hashedGivenPassword = password_hash($givenPassword, PASSWORD_DEFAULT);

        // TODO: save in database

        Session::set('username', $givenUsername);
    }
}