<?php

require_once __DIR__ . '/../utils/Response.php';
require_once __DIR__ . '/../utils/Request.php';
require_once __DIR__ . '/../utils/Sitemap.php';
require_once __DIR__ . '/../utils/Robots.php';
require_once __DIR__ . '/../logs/Logger.php';
require_once __DIR__ . '/../Env.php';

class DeployController
{
    public static function deploy(): void
    {
        if (
            Request::getUriWithoutParams() === '/deploy'
            && Request::isPost()
            && Request::getFromServer('HTTP_X_GITLAB_INSTANCE') === Env::get('X_GITLAB_INSTANCE')
            && Request::getFromServer('HTTP_X_GITLAB_TOKEN') === Env::get('X_GITLAB_TOKEN')
	    ) {
            self::pullChangesFromGit();
            Robots::createRobotsFile();
            Sitemap::createSitemapFile();
            Response::responseOK();
        } else {
            Response::responseNotFound();
        }
    }

    private static function pullChangesFromGit(): void
    {
        shell_exec('cd ' . __DIR__ . '/../ && git pull origin production');
        Logger::log('[DEPLOY] Changes have been applied.');
    }
}
