<?php

require_once __DIR__ . '/../views/View.php';
require_once __DIR__ . '/../utils/Request.php';

class IndexController
{
    public static function index(): void
    {
        View::build('index/index.php', [
            'metta-canonical' => Request::getHostWithSchema()
        ]);
    }
}